from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework import routers

from checks import views as checkviews

router = routers.DefaultRouter()
router.register(r'checks', checkviews.CheckViewSet, base_name='check')
router.register(r'checktypes', checkviews.CheckTypeViewSet, base_name='checktype')

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'monmon.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(router.urls)),
)
