================
 Mon Mon Server
================

Moving Parts
------------

We use `Django <https://www.djangoproject.com/>`_ for web facing stuff
and database access, and we use `Celery <http://celery.readthedocs.org/en/latest/>`_
for job scheduling.

Four processes make up a fully functional development environment:

1. Django server
2. A RabbitMQ server
3. Celery beat (periodic job scheduler)
4. Celery worker (job executor)

Hacking
-------

Those four processes are kind of a pain to manage manually, so we have
a couple of options to make hacking on the project quick and easy.

If you dig `Docker <https://www.docker.com/>`_, you should dig
`Docker Compose <https://docs.docker.com/compose/>`_.  Assuming you
have both installed, you can start all four development processes
with:

    docker-compose build
    docker-compose run web ./manage.py migrate
    docker-compose up

Let's say you'd rather use Vagrant.  That's easy too:

    vagrant up
    vagrant ssh master
