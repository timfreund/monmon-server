from mmcl import CheckRequest, execute
from celery import shared_task

__all__ = ['execute_check_distributed', 'execute_check_local']

@shared_task
def execute_check_distributed(*args, **kwargs):
    print("execute_check_local")
    print(args)
    print(kwargs)

@shared_task
def execute_check_local(*args, **kwargs):
    print("execute_check_local")
    print(args)
    print(kwargs)

    request = CheckRequest(check_type=kwargs['check_type'],
                           **kwargs['parameters'])
    response = execute(request)
    return response
