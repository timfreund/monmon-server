from checks.models import *
from rest_framework import serializers

__all__ = [
    'CheckParameterSerializer',
    'CheckSerializer',
    'CheckTypeMetaparameterSerializer',
    'CheckTypeSerializer',
]

class CheckParameterSerializer(serializers.ModelSerializer):
    class Meta:
        model = CheckParameter

class CheckSerializer(serializers.ModelSerializer):
    class Meta:
        model = Check

class CheckTypeMetaparameterSerializer(serializers.ModelSerializer):
    class Meta:
        model = CheckTypeMetaparameter

class CheckTypeSerializer(serializers.ModelSerializer):
    # metaparameters = CheckTypeMetaparameterSerializer(many=True)

    class Meta:
        model = CheckType


