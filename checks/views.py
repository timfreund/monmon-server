from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet

from checks.models import *
from checks.rest import *

class CheckTypeViewSet(ModelViewSet):
    serializer_class = CheckTypeSerializer
    queryset = CheckType.objects.all()

    def create(self, request, *args, **kwargs):
        resp = ModelViewSet.create(self, request, *args, **kwargs)
        if resp.status_code == 201:
            ct_id = resp.data['id']
            for mp in request.data['metaparameters']:
                mp['check_type'] = ct_id
                ctms = CheckTypeMetaparameterSerializer(data=mp)
                ctms.is_valid()
                ctm = ctms.create(ctms.validated_data)
                if not 'metaparameters' in resp.data:
                    resp.data['metaparameters'] = []
                resp.data['metaparameters'].append(ctms.to_representation(ctm))
        return resp

class CheckViewSet(ModelViewSet):
    serializer_class = CheckSerializer
    queryset = Check.objects.all()


