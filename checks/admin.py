from django.contrib import admin
from checks.models import (
    Check,
    CheckType,
    CheckParameter,
    CheckTypeMetaparameter,
)

class CheckParameterInline(admin.TabularInline):
    model = CheckParameter

class CheckAdmin(admin.ModelAdmin):
    inlines = [
        CheckParameterInline,
    ]

class CheckTypeMetaparameterInline(admin.TabularInline):
    model = CheckTypeMetaparameter

class CheckTypeAdmin(admin.ModelAdmin):
    inlines = [
        CheckTypeMetaparameterInline,
    ]

# Register your models here.
admin.site.register(Check, CheckAdmin)
admin.site.register(CheckType, CheckTypeAdmin)
admin.site.register(CheckParameter)
admin.site.register(CheckTypeMetaparameter)
