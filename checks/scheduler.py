import logging
import monmon.settings

from celery.beat import Scheduler, ScheduleEntry
from celery import schedules
from datetime import datetime, date, timedelta, timezone
from django.core import serializers
from django.db.models import Max

from checks.models import *
from checks.tasks import *

logger = logging.getLogger(__name__)

class CheckScheduler(Scheduler):
    _last_read = None
    _schedule = None
    _execution_function_name = None

    def __init__(self, *args, **kwargs):
        model = getattr(monmon.settings, 'CHECK_EXECUTION_MODEL', 'local')
        self._execution_function_name = 'checks.tasks.execute_check_%s' % model
        Scheduler.__init__(self, *args, **kwargs)

    def read_schedule_entries(self):
        self._last_read = datetime.now(timezone.utc)
        schedule = {}
        for check in Check.objects.all():
            check_type = check.check_type.code_name
            parameters = {}
            for cp in check.parameters.all():
                parameters[cp.meta.name] = cp.value

            se = ScheduleEntry(name=check.name,
                               task=self._execution_function_name,
                               schedule=schedules.schedule(timedelta(seconds=check.frequency)),
                               kwargs={'check_id': check.id,
                                       'check_type': check_type,
                                       'parameters': parameters},
                           )
            schedule[check.name] = se
        return schedule

    def schedule_changed(self):
        mru = Check.objects.all().aggregate(Max('updated_at'))['updated_at__max']
        if not self._last_read or not mru or mru > self._last_read:
            logger.debug("schedule has changed (%s > %s)" % (mru, self._last_read))
            return True
        return False

    def serialize_check(self, check):
        return serializers.serialize('json', [check])

    def setup_schedule(self):
        self.install_default_entries(self.schedule)
        self.update_from_dict(self.app.conf.CELERYBEAT_SCHEDULE)

    @property
    def schedule(self):
        if self.schedule_changed():
            self._schedule = self.read_schedule_entries()
        else:
            logger.debug("Schedule is still up to date")
        return self._schedule
