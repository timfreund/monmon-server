import datetime

from django.db import models

__all__ = [
    'Check',
    'CheckParameter',
    'CheckType',
    'CheckTypeMetaparameter',
]

class CheckType(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    code_name = models.CharField(max_length=128, unique=True)
    display_name = models.CharField(max_length=128)

    def __str__(self):
        return self.display_name


class CheckTypeMetaparameter(models.Model):
    types = (
        ('INT', 'Integer'),
        ('STR', 'String'),
        ('DATE', 'Date'),
        ('BOOL', 'Boolean'),
    )
    check_type = models.ForeignKey(CheckType, related_name='metaparameters')
    name = models.CharField(max_length=128)
    display_name = models.CharField(max_length=128)
    description = models.CharField(max_length=2048)
    parameter_type = models.CharField(max_length=16, choices=types)

    def __str__(self):
        return "%s.%s" % (self.check_type.display_name, self.display_name)


class Check(models.Model):
    check_type = models.ForeignKey(CheckType, related_name='checks')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=128, default="")
    frequency = models.IntegerField(default=60)

    def __str__(self):
        return self.name


class CheckParameter(models.Model):
    # Parent cannot be named check because of Djangos ORM wanting to map the
    # `Check` model to a `check()` method to this object when mapping
    # its relationships.
    parent = models.ForeignKey(Check, related_name='parameters')
    meta = models.ForeignKey(CheckTypeMetaparameter, related_name='+')
    updated_at = models.DateTimeField(auto_now=True)
    value = models.CharField(max_length=2048)

    @property
    def name(self):
        return self.meta.name

    # TODO CheckParameter.parent's ForeignKey def can accept limit_choices_to
    # but we don't have any context to correctly limit the choices.  If we
    # have access to self, this is what we'd want:
    #
    # def parent_limiter(self):
    #     return CheckTypeMetaparameter.objects.filter(check_type_id=parent.check_type.id)

    def __str__(self):
        return str(self.name)
