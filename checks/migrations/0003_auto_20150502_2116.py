# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def set_meta_id(apps, schema_editor):
    CheckParameter = apps.get_model("checks", "CheckParameter")
    for cparm in CheckParameter.objects.all():
        cparm.meta = cparm.name
        cparm.save()


class Migration(migrations.Migration):

    dependencies = [
        ('checks', '0002_auto_20150330_0605'),
    ]

    operations = [
        migrations.AddField(
            model_name='checkparameter',
            name='meta',
            field=models.ForeignKey(default=-1, to='checks.CheckTypeMetaparameter', related_name='+'),
            preserve_default=False,
        ),
        migrations.RunPython(set_meta_id),
    ]
