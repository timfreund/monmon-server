# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('checks', '0003_auto_20150502_2116'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='checkparameter',
            name='name',
        ),
        migrations.AlterField(
            model_name='check',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='check',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='checkparameter',
            name='updated_at',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='checktype',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
