# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def check_for_conflicts(apps, schema_editor):
    CheckType = apps.get_model("checks", "CheckType")
    duplicates = []
    for ct in CheckType.objects.all():
        if CheckType.objects.filter(code_name=ct.code_name).count() > 1:
            if ct.code_name not in duplicates:
                duplicates.append(ct.code_name)
    if duplicates:
        raise Exception("Duplicate CheckType.code_name values exist: %s" % 
                        ", ".join(duplicates))

class Migration(migrations.Migration):

    dependencies = [
        ('checks', '0004_auto_20150502_2128'),
    ]

    operations = [
        migrations.RunPython(check_for_conflicts),
        migrations.AlterField(
            model_name='checktype',
            name='code_name',
            field=models.CharField(unique=True, max_length=128),
        ),
    ]
