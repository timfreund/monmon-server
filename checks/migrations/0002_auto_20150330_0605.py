# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


def current_time():
    return datetime.datetime.utcnow()


class Migration(migrations.Migration):

    dependencies = [
        ('checks', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CheckParameter',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('updated_at', models.DateTimeField(default=current_time, auto_now=True)),
                ('value', models.CharField(max_length=2048)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CheckTypeMetaparameter',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128)),
                ('display_name', models.CharField(max_length=128)),
                ('description', models.CharField(max_length=2048)),
                ('parameter_type', models.CharField(choices=[('INT', 'Integer'), ('STR', 'String'), ('DATE', 'Date'), ('BOOL', 'Boolean')], max_length=16)),
                ('check_type', models.ForeignKey(to='checks.CheckType', related_name='metaparameters')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='checkparameter',
            name='name',
            field=models.OneToOneField(to='checks.CheckTypeMetaparameter'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='checkparameter',
            name='parent',
            field=models.ForeignKey(to='checks.Check', related_name='parameters'),
            preserve_default=True,
        ),
        migrations.RemoveField(
            model_name='check',
            name='parameters',
        ),
        migrations.RemoveField(
            model_name='checktype',
            name='metaparameters',
        ),
        migrations.AddField(
            model_name='check',
            name='created_at',
            field=models.DateTimeField(default=current_time, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='check',
            name='name',
            field=models.CharField(default='', max_length=128),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='check',
            name='updated_at',
            field=models.DateTimeField(default=current_time, auto_now=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='checktype',
            name='created_at',
            field=models.DateTimeField(default=current_time, auto_now_add=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='check',
            name='check_type',
            field=models.ForeignKey(to='checks.CheckType', related_name='checks'),
            preserve_default=True,
        ),
    ]
