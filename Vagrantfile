# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|

  config.vm.box = "ubuntu/trusty64"
  # config.vm.network "public_network"
  config.vm.synced_folder ".", "/local", type: "nfs"

  config.vm.provision "shell", inline: <<-SHELL
    wget https://apt.puppetlabs.com/puppetlabs-release-precise.deb
    dpkg -i puppetlabs-release-precise.deb
    apt-get update
    apt-get install -y build-essential git emacs vim
    # apt-get install -y puppet

    echo '192.168.33.10 master.vm master' >> /etc/hosts
    echo '192.168.33.11 rabbitmq.vm rabbitmq' >> /etc/hosts
  SHELL

  config.vm.define :master do |box|
      box.vm.hostname = "master.vm"
      box.vm.network "private_network", ip: "192.168.33.10"
      box.vm.network "forwarded_port", guest: 8000, host: 8000
      box.vm.provider "virtualbox" do |vb|
        vb.memory = "1024"
      end

      box.vm.provision "shell", inline: <<-SHELL
        ln -s /local /home/vagrant/code

        # Install Python 3, virtualenv, and pip
        apt-get install -y python3 python-pip
        pip install virtualenvwrapper
      SHELL

      box.vm.provision "shell", inline: <<-SHELL, privileged: false
        echo 'export WORKON_HOME=$HOME/.virtualenvs' >> /home/vagrant/.bashrc
        echo 'export PROJECT_HOME=$HOME/Devel' >> /home/vagrant/.bashrc
        echo 'source /usr/local/bin/virtualenvwrapper.sh' >> /home/vagrant/.bashrc
        echo "workon monmon" >> /home/vagrant/.bashrc

        export WORKON_HOME=$HOME/.virtualenvs
        export PROJECT_HOME=$HOME/Devel
        source /usr/local/bin/virtualenvwrapper.sh

        # Install monmon dependancies
        mkvirtualenv monmon -r /home/vagrant/code/requirements.txt -p $(which python3)
      SHELL
  end

  config.vm.define :rabbitmq do |box|
      box.vm.hostname = "rabbitmq.vm"
      box.vm.network "private_network", ip: "192.168.33.11"
      box.vm.network "forwarded_port", guest: 5672, host: 5672
      box.vm.provider "virtualbox" do |vb|
        vb.memory = "512"
      end

      box.vm.provision "shell", inline: <<-SHELL
        wget https://www.rabbitmq.com/rabbitmq-signing-key-public.asc
        apt-key add rabbitmq-signing-key-public.asc
        apt-get install -y rabbitmq-server
        service rabbitmq-server start
      SHELL
  end

end
